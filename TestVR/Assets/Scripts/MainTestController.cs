﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainTestController : MonoBehaviour
{
	public int QuestionCount = 5;
	public Text questionText;
	public CanvasGroup buttonsCanvas;
	public GameObject EndPanel;
	public Image pointerProgressImage;

	private List<QuestionEntity> _questionsList;
	private int _currentQuestionIndex = -1;
	private QuestionEntity _currentQuestion;


	private MainVRController _vrController;

	// Use this for initialization
	void Start()
	{
		//buttonsCanvas.interactable = false;
		_vrController = FindObjectOfType<MainVRController>();
		_vrController.EnableVr();
		LoadQuestions();
	}

	#region Loading Questions
	void LoadQuestions()
	{
		QuestionsLoader questionLoader = new QuestionsLoader();
		questionLoader.onLoadQuestionsCallback += OnQuestionLoaded;
		StartCoroutine(questionLoader.LoadQuestions(QuestionCount));
	}

	void OnQuestionLoaded(List<QuestionEntity> questions)
	{
		if (questions == null || questions.Count == 0)
		{
			_questionsList = GetLocalQuestions();
		}
		else
		{
			_questionsList = questions;
		}

		TryToNextQuestion();
	}

	private List<QuestionEntity> GetLocalQuestions()
	{
		List<QuestionEntity> result = new List<QuestionEntity>();

		result.Add(new QuestionEntity("Do you believe that Earth is flat?"));
		result.Add(new QuestionEntity("Do you believe that Earth is standing on the backs of four elephants?"));
		result.Add(new QuestionEntity("Do you believe those elephants are standing on the back of the giant turtle?"));
		result.Add(new QuestionEntity("Do you think that that tutrle is female?"));
		result.Add(new QuestionEntity("Haven't you already figure out whom we are referencing here?"));
		result.Add(new QuestionEntity("Have you ever heard of Sir Terry Pratchett?"));

		return result;
	}
	#endregion

	public void TryToNextQuestion()
	{
		if (_currentQuestionIndex == _questionsList.Count - 1)
		{
			EndOfQuiz();
		}
		else
		{
			NextQuestion();
		}
	}

	public void TryingToPress(float progress)
	{
		pointerProgressImage.fillAmount = progress;
	}

	public void SetAnswer(bool isYesButton)
	{
		pointerProgressImage.fillAmount = 0;
		if (EndPanel.activeSelf)
		{
			if (isYesButton)
			{
				SceneManager.LoadScene(0);
			}
			else
			{
				Application.Quit();
			}
		}
		else
		{
			_currentQuestion.SetAnswer(isYesButton);
			TryToNextQuestion();
		}
	}


	private void NextQuestion()
	{
		_currentQuestion = _questionsList[++_currentQuestionIndex];
		questionText.text = _currentQuestion.Body;
		buttonsCanvas.interactable = true;
	}



	private string TryToGetIngameEmail()
	{
		return "bch,betterhave@mymoney";
	}

	public void EndOfQuiz()
	{
		Debug.Log("Quiz ended");
		Debug.Log("========= Results =======");
		foreach (var item in _questionsList)
		{
			Debug.Log(item.Body + "             " + item.Answer);
		}
		Debug.Log("========= Results =======");
		//buttonsCanvas.interactable = false;
		EndPanel.SetActive(true);

		EnterEmail();
	}

	private void EnterEmail()
	{
		string email = TryToGetIngameEmail();	

		if (email == null)
		{
			_vrController.DisableVr();
		}
			
		SceneManager.LoadScene("InputScene");
	}

	private void OnEmailSent(bool succeed)
	{
		Debug.Log("email send succesfully " + succeed);
	}

}
