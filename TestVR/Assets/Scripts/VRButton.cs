﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Collider), typeof(EventTrigger))]
public class VRButton : MonoBehaviour
{
	public bool isYesButton = true;

	public Material inactiveMaterial;
	public Material gazedAtMaterial;

	public float gazingDurationToActivate = 2f;
	private float _gazedDuration = 0;

	private bool _didPressClick = false;
	private MainTestController _mainController;
	private bool _isGazedAt;

	Renderer CurrentRenderer
	{ 
		get
		{
			if (_renderer == null)
			{
				_renderer = GetComponent<Renderer>();
			}
			return _renderer;
		}
	}
	private Renderer _renderer;

	void Start()
	{
		_renderer = GetComponent<Renderer>();
		_mainController = FindObjectOfType<MainTestController>();
		SetGazedAt(false);
	}

	public void SetGazedAt(bool gazedAt)
	{
		_isGazedAt = gazedAt;

		if (!gazedAt)
		{
			_didPressClick = false;
			_gazedDuration = 0;
			_mainController.TryingToPress(0);
		}

		if (inactiveMaterial != null && gazedAtMaterial != null)
		{
			CurrentRenderer.material = gazedAt ? gazedAtMaterial : inactiveMaterial;
			return;
		}
		CurrentRenderer.material.color = gazedAt ? Color.green : Color.red;
	}

	public void Reset()
	{
		
	}

	public void Recenter()
	{
		#if !UNITY_EDITOR
			GvrCardboardHelpers.Recenter();
		#else
		if (GvrEditorEmulator.Instance != null)
		{
			GvrEditorEmulator.Instance.Recenter();
		}
		#endif  // !UNITY_EDITOR
	}

	void Update()
	{
		if (!_didPressClick && _isGazedAt)
		{
			_gazedDuration += Time.deltaTime;

			if (_gazedDuration > gazingDurationToActivate)
			{
				OnButtonClick(isYesButton);
			} else
			{
				_mainController.TryingToPress(_gazedDuration/gazingDurationToActivate);
			}
		}
	}

	public void OnButtonClick(bool answer)
	{
		_didPressClick = true;
		Debug.Log(answer+"  button pressed");
		_mainController.SetAnswer(answer);

	}
}