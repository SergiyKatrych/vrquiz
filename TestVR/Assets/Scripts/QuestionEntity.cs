﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionEntity
{
	public int id;
	public string _questionBody;
	public bool answer;
	public string createdAt, updatedAt;


	public string Body{ get { return _questionBody; } }

	bool _answer;
	public bool Answer{ get { return _answer; } }

	public QuestionEntity()
	{
		
	}

	public QuestionEntity(string body)
	{
		_questionBody = body;
	}

	public void SetAnswer(bool answer)
	{
		_answer = answer;
	}
}
