﻿using UnityEngine;
using System;

public abstract class AbstractWWWLoader
{
	public enum WWWError
	{
		TimeOutError,							//www request time is longer than timeout
		NotJsonCompatibleDataError,				//www request got bad data from the server, not json compatible
		ConnectionError							//Error when problem with internet connection, access to server or script, etc
	}

	Action<WWWError> onLoadErrorCallback;

	protected bool CheckWWW( WWW w, bool requireJson = true, Action<WWWError> errorCallback = null)
	{
		onLoadErrorCallback = errorCallback;
		if (w == null)
		{
			CallErrorEvent(WWWError.TimeOutError, "WWW timeout");
			return false;
		}
		if (w.error != null)
		{
			CallErrorEvent(WWWError.ConnectionError, w.error);
			return false;
		}
		if (requireJson && w.text.Trim().Length != 0 && !Utils.IsValidJson(w.text))
		{
			CallErrorEvent(WWWError.NotJsonCompatibleDataError, w.text);
			return false;
		}
		return true;
	}

	protected void CallErrorEvent(WWWError errorType, string errorMessage)
	{
		Debug.LogError("ERROR [" + errorType + "]: " + errorMessage);

		if (onLoadErrorCallback != null)
		{
			onLoadErrorCallback(errorType);
		}
		else
		{
			OnErrorEvent(errorType);
		}
	}

	protected	abstract void OnErrorEvent(WWWError errorType);
}
