﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class QuestionsLoader : AbstractWWWLoader
{
	private List<QuestionEntity> _questions;
	public Action<List<QuestionEntity>> onLoadQuestionsCallback;

	public IEnumerator LoadQuestions(int count)
	{
		WWWForm form = new WWWForm();

		WaitForWWWTimeout wwwTimeout = new WaitForWWWTimeout(new WWW(Constants.URL.LoadQuestions+count), Constants.WWWCustomTimeOut);
		yield return wwwTimeout;
		WWWOnLoadEvent((WWW)wwwTimeout.Current);
	}

	private void WWWOnLoadEvent(WWW w)
	{
		if (CheckWWW(w))
		{
			Debug.Log(w.text);
			ParseQuestionsJSON(w.text);
			if (onLoadQuestionsCallback != null)
			{
				onLoadQuestionsCallback(_questions);
			}
		}
	}
		
	private void ParseQuestionsJSON(string json)
	{
		JSONObject data = new JSONObject(json);
		_questions = new List<QuestionEntity>();
		for (int i = 0; i < data.Count; i++)
		{
			QuestionEntity question = new QuestionEntity();


			question.id = int.Parse(data[i][Constants.JsonKeys.Id].ToString());
			question._questionBody = data[i][Constants.JsonKeys.Question].ToString();
			question.createdAt = data[i][Constants.JsonKeys.CreatedAt].ToString();
			question.updatedAt = data[i][Constants.JsonKeys.UpdatedAt].ToString();

			_questions.Add(question);
		}
	}

	#region implemented abstract members of AbstractWWWLoader

	protected override void OnErrorEvent(WWWError errorType)
	{
		Debug.LogError("here we are");
		if (onLoadQuestionsCallback != null)
		{
			onLoadQuestionsCallback(null);
		}
	}

	#endregion
}
