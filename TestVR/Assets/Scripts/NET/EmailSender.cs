﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.XR;
using System.Text.RegularExpressions;
using System.Globalization;
using UnityEngine.SceneManagement;

public class EmailSender : MonoBehaviour
{
	public Action<bool> storedEmailCallback;
	public InputField emailInput;
	public Button sendButton;

	public GameObject successfullPanel;

	public void Start()
	{
		successfullPanel.SetActive(false);
		emailInput.text = PlayerPrefs.GetString("email", "");
		StartCoroutine(LoadDevice("", false));
	}

	IEnumerator LoadDevice(string newDevice, bool enable)
	{
		XRSettings.LoadDeviceByName(newDevice);
		yield return null;
		XRSettings.enabled = enable;
	}

	public void OnValueChanged(string temp)
	{
		OnEndEdit(temp);
	}

	public void OnEndEdit(string temp)
	{

		sendButton.interactable = IsValidEmail(temp);

		emailInput.text = temp;
	}


	bool invalid = false;
	public bool IsValidEmail(string strIn)
	{
		invalid = false;
		if (String.IsNullOrEmpty(strIn))
			return false;

		// Use IdnMapping class to convert Unicode domain names.
		strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper);
		if (invalid) 
			return false;

		// Return true if strIn is in valid e-mail format.
		return Regex.IsMatch(strIn, 
			@"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" + 
			@"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$", 
			RegexOptions.IgnoreCase);
	}

	private string DomainMapper(Match match)
	{
		// IdnMapping class with default property values.
		IdnMapping idn = new IdnMapping();

		string domainName = match.Groups[2].Value;
		try {
			domainName = idn.GetAscii(domainName);
		}
		catch (ArgumentException) {
			invalid = true;      
		}      
		return match.Groups[1].Value + domainName;
	}

	public void SendEmail()
	{
		PlayerPrefs.SetString("email", emailInput.text);
		StartCoroutine(SendEmail(emailInput.text, OnEmailSent));
	}



	public IEnumerator SendEmail(string email, Action<bool> callback)
	{
		storedEmailCallback = callback;
		WWWForm form = new WWWForm();

		form.AddField(Constants.JsonKeys.UserEmail, email);

		var postRequest = UnityWebRequest.Post(Constants.URL.StoreEmail, form);
		postRequest.timeout = Constants.WWWCustomTimeOut;

		yield return postRequest.SendWebRequest();

		if (postRequest.isNetworkError || postRequest.isHttpError || postRequest.responseCode != 200)
		{
			Debug.Log("postRequest.responseCode "+postRequest.responseCode);
			OnErrorEvent(postRequest.error);
			storedEmailCallback(false);
		}
		else
		{
			storedEmailCallback(true);
		}
	}

	private void OnEmailSent(bool succeed)
	{
		if (succeed)
		{
			successfullPanel.SetActive(true);
		}
		Debug.Log("email send succesfully " + succeed);
	}

	bool ParseAnswer(string text)
	{
		JSONObject answer = new JSONObject(text);

		if (answer.GetField(Constants.JsonKeys.Answer).ToString() == "true")
		{
			return true;
		}
		else
			return false;
	}

	protected void OnErrorEvent(string errorType)
	{
		Debug.LogError("here we are");
		Debug.LogError(errorType);
//		if (onLoadQuestionsCallback != null)
//		{
//			onLoadQuestionsCallback(null);
//		}
	}



	public void Restart()
	{
		SceneManager.LoadScene("MainGameplay");
	}

	public void Quit()
	{
		Application.Quit();
	}
}
