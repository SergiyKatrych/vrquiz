﻿using UnityEngine;
using System.Collections;

public class WaitForWWWTimeout : IEnumerator
{
	private float timer;
	private float timeOut;
	private WWW www;

	public object Current
	{
		get { return www; }
	}

	public WaitForWWWTimeout(WWW www, float timeOut)
	{
		timer = 0;
		this.timeOut = timeOut;
		this.www = www;
	}

	public bool MoveNext()
	{
		if (www.isDone)
		{
			return false;
		}
		if (timer >= timeOut)
		{
			www.Dispose();
			www = null;
			return false;
		}
		timer += Time.deltaTime;
		return true;
	}

	public void Reset() { }
}
