﻿using UnityEngine;

public static class Constants
{
	public static string Version = "0.01";

	public static int AnswersMaxCount = 4;
	public static int BossTestLevelsQuantity = 3;
	public static int WWWCustomTimeOut = 10;

	public struct Colors
	{
		public struct UI
		{
			public static Color32 Green = new Color32(103, 255, 93, 255);
			public static Color32 Blue = new Color32(62, 241, 255, 255);
			public static Color32 Red = new Color32(255, 62, 136, 126);
		}
	}


	#region Audio
	public struct Audio
	{
		public struct OST
		{
			//public static string MainMenu = "world_OST";
			public static string MainMenu = "main_menu_sound";
		}

		public struct Sounds
		{
			public const string ButtonClick = "tap";

			public struct Feedback
			{
				public const string Positive = "answer_positive";
				public const string Negative = "answer_negative";
				public const string QuizFinished = "win_sifi";
			}

			public struct Transition
			{
				public const string Fade = "Transition/Fade";

			}

			public struct Popup
			{
				public const string Show = "Popup/popup_show";
				public const string Hide = "Popup/popup_hide";
			}
		}
	}

	#endregion


	#region JsonKeys
	public struct JsonKeys
	{
		public static string Id = "id";
		public static string Question = "question";
		public static string Answer = "answer";
		public static string CreatedAt = "created_at";
		public static string UpdatedAt = "updated_at";

		public static string UserEmail = "email";
	}
	#endregion

	#region URL
	public struct URL
	{
		private static string DebugDomen = "http://188.226.138.173/api/";
		private static string ReleaseDomen = "http://188.226.138.173/api/";

		private static string CurrentDomen
		{
			get
			{
				return false ? DebugDomen : ReleaseDomen;
			}
		}

		public static string LoadQuestions = CurrentDomen + "getQuestions/";
		public static string StoreEmail = CurrentDomen + "addEmail";

	}
	#endregion
}