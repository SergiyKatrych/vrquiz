﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class MainVRController : MonoBehaviour
{
	//
	// "daydream" or "cardboard" (both lowercase).
	//
	public Camera monoCamera;
	string[] supportedDevices;

	bool IsVREnabled = false;

	// Use this for initialization
	void Start()
	{
		//	supportedDevices = XRSettings.supportedDevices;
		
	}

	public void ChangeVRState()
	{
		if (XRSettings.enabled)
		{
			DisableVr();
		}
		else
		{
			EnableVr();
		}
	}

	public void EnableVr()
	{
		if (!XRSettings.enabled)
		{
			StartCoroutine(LoadDevice("daydream", true));
		}
	}

	public void DisableVr()
	{
		Debug.Log("Disable this shit");
		StartCoroutine(LoadDevice("", false));
	}

	IEnumerator LoadDevice(string newDevice, bool enable)
	{
		XRSettings.LoadDeviceByName(newDevice);
		yield return null;
		XRSettings.enabled = enable;

		if (!enable)
		{
			ResetCameras();
		}
	}

	void Update()
	{
		//Debug.Log(" sss "+XRSettings.enabled + GvrVRHelpers.GetHeadRotation());
		#if !UNITY_EDITOR
		if (!XRSettings.enabled)
		{
			monoCamera.transform.rotation = UnityEngine.XR.InputTracking.GetLocalRotation(XRNode.CenterEye);
			monoCamera.ResetAspect();
		}
		#endif
	}

	// Resets local rotation and calls `ResetAspect()` on all enabled VR cameras.
	void ResetCameras()
	{
		// Camera looping logic copied from GvrEditorEmulator.cs
		for (int i = 0; i < Camera.allCameras.Length; i++)
		{
			Camera cam = Camera.allCameras[i];
			if (cam.enabled && cam.stereoTargetEye != StereoTargetEyeMask.None)
			{

				// Reset local rotation. (Only required if you change the local rotation while in non-VR mode.)
				cam.transform.localRotation = Quaternion.identity;

				// Reset local position. (Only required if you change the local position while in non-VR mode.)
				cam.transform.localPosition = Vector3.zero;

				// Reset aspect ratio based on normal (non-VR) screen size.
				// Required in certain versions of Unity, see github.com/googlevr/gvr-unity-sdk/issues/628
				cam.ResetAspect();

				// Don't need to reset camera `fieldOfView`, since it's restored to the original value automatically.
			}
		}
	}
}
