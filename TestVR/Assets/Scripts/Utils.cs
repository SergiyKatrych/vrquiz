﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

static public class Utils
{
	public static Vector2 ConvertPoint(Vector2 poin, Camera Cam)
	{
		return Cam.ScreenToWorldPoint(Camera.main.WorldToScreenPoint(poin));
	}


	public static T ToEnum<T>(this string value)
	{
		return (T)System.Enum.Parse(typeof(T), value, true);
	}

	public static string GetTimeLeft(int timeLeft)
	{
		string s = "";
		string t = "minutes";
		int time = timeLeft;
		int tt = timeLeft / 60;
		if (tt != 0)
		{
			t = "hours";
			time = tt;
			tt = timeLeft / 60 / 24;
			if (tt != 0)
			{
				if (tt == 1)
					t = "day";
				else
					t = "days";
				time = tt;
				int minutes_left = timeLeft % (60 * 24);
				if (minutes_left != 0)
				{
					int th = minutes_left / 60;
					if (th != 0)
					{
						if (th > 1)
							t += " " + th.ToString() + " hours";
						else
							t += " and " + th.ToString() + " hour";
					}
				}

			}

		}


		s = time.ToString() + " " + t + " " + "left";
		return s;
	}

	public static string GetTimeFinished(int timeLeft)
	{
		string s = "";
		string t = "minutes ago";
		int time = timeLeft;
		int tt = timeLeft / 60;
		if (tt != 0)
		{
			t = "hours ago";
			time = tt;
			tt = timeLeft / 60 / 24;
			if (tt != 0)
			{
				if (tt == 1)
					//t = "day ago";
					t = "day" + " " + "ago";


				else
					//t = "days ago";
					t = "days" + " " + "ago";

				time = tt;
			}
		}
		else
		{
			if (time == 0)
			{
				t = "just now";
				s = t;
				return s;
			}
		}
		s = time.ToString() + " " + t;
		return s;
	}

	public static int GetBiggestElement(int[] array)
	{
		int res = 0;
		for (int i = 0; i < array.Length; i++)
		{
			if (array[i] > res)
			{
				res = array[i];
			}
		}
		return res;
	}

	static public T GetRandomEnum<T>()
	{
		System.Array A = System.Enum.GetValues(typeof(T));
		T V = (T)A.GetValue(UnityEngine.Random.Range(0, A.Length));
		return V;
	}

	static public bool RandomBool()
	{
		return Random.value > 0.5f;
	}

	static public List<T> Shuffle<T>(this List<T> list)
	{
		int n = list.Count;
		List<T> new_list = list.CopyList();
		while (n > 0)
		{
			int k = Random.Range(0, n);
			T item = new_list[n - 1];
			new_list[n - 1] = new_list[k];
			new_list[k] = item;
			n--;
		}
		return new_list;
	}

	static public T[] Shuffle<T>(this T[] array)
	{
		int n = array.Length;
		T[] new_array = array.CopyArray();
		while (n > 0)
		{
			int k = Random.Range(0, n);
			T item = new_array[n - 1];
			new_array[n - 1] = new_array[k];
			new_array[k] = item;
			n--;
		}
		return new_array;
	}

	static public List<T> CopyList<T>(this List<T> list)
	{
		int n = list.Count;
		List<T> new_list = new List<T>();
		;
		for (int i = 0; i < n; i++)
		{
			new_list.Add(list[i]);
		}
		return new_list;
	}

	static public T[] CopyArray<T>(this T[] array)
	{
		int n = array.Length;
		T[] new_array = new T[n];
		for (int i = 0; i < n; i++)
		{
			new_array[i] = array[i];
		}
		return new_array;
	}

	static public T GetRandomElement<T>(this List<T> list)
	{
		return list[Random.Range(0, list.Count)];
	}


	static public T GetRandomElement<T>(this T[] array)
	{
		return array[Random.Range(0, array.Length)];
	}

	static public string GetUID()
	{
		string UID = System.Guid.NewGuid().ToString();
		if (PlayerPrefs.HasKey("UID"))
		{
			UID = PlayerPrefs.GetString("UID");
		}
		else
		{
			PlayerPrefs.SetString("UID", UID);
			PlayerPrefs.Save();
		}
		return UID;
	}

	public static bool JsonDataContainsKey(JSONObject data, string key)
	{
		bool result = false;
		if (data == null)
			return result;
		if (!data.IsObject)
		{
			return result;
		}
		IDictionary tdictionary = data as IDictionary;
		if (tdictionary == null)
			return result;
		if (tdictionary.Contains(key))
		{
			result = true;
		}
		return result;
	}

	public static bool IsValidJson(string strInput)
	{
		strInput = strInput.Trim();
		if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
			(strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
		{
				var obj = new JSONObject(strInput);
				return true;
		}
		else
		{
			return false;
		}
	}

	public static bool IsAbsoluteUrl(string url)
	{
		return System.Uri.IsWellFormedUriString(url, System.UriKind.Absolute);
	}
}


public static class WeightedRandom
{
	public static float[] CalcLookups(float[] weights)
	{
		float total_weight = 0;
		for (int i = 0; i < weights.Length; i++)
		{
			total_weight += weights[i];
		}
		float[] lookups = new float[weights.Length];
		for (int i = 0; i < weights.Length; i++)
		{
			lookups[i] = (weights[i] / total_weight) + (i == 0 ? 0 : lookups[i - 1]);
		}
		return lookups;
	}

	private static int binary_search(float needle, float[] lookups)
	{
		int high = lookups.Length - 1;
		int low = 0;
		int probe = 0;
		if (lookups.Length < 2)
		{
			return 0;
		}
		while (low < high)
		{
			probe = (int)((high + low) / 2);

			if (lookups[probe] < needle)
			{
				low = probe + 1;
			}
			else if (lookups[probe] > needle)
			{
				high = probe - 1;
			}
			else
			{
				return probe;
			}
		}

		if (low != high)
		{
			return probe;
		}
		else
		{
			return (lookups[low] >= needle) ? low : low + 1;
		}
	}


	public static int RandomW(float[] weights)
	{
		weights = CalcLookups(weights);

		if (weights.Length > 0)
			return binary_search(Random.value, weights);
		else
			return -1;
	}


	public static int PrecalculatedRandomW(float[] lookups) //used for getting random on the same set to optimise time by excludinc CalcLookups from each cycle of getting random
	{
		if (lookups.Length > 0)
			return binary_search(Random.value, lookups);
		else
			return -1;
	}
}